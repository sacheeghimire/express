const products = [
  {
    id: 1,
    title: "MacBook Pro",
    category: "Laptops",
    price: 100000.0,
    description: "A high-performance laptop.",
    manufactureDate: "2023-05-15T08:30:00",
    isAvailable: true,
  },
  {
    id: 2,
    title: "Nike",
    category: "Running Shoes",
    price: 5000,
    description: "Running shoes designed for speed and comfort.",
    manufactureDate: "2023-02-20T14:45:00",
    isAvailable: true,
  },
  {
    id: 3,
    title: "Python",
    category: "Books",
    price: 500,
    description: "A language for AI",
    manufactureDate: "1925-04-10T10:10:00",
    isAvailable: false,
  },
  {
    id: 4,
    title: "Javascript",
    category: "Books",
    price: 700,
    description: "A language for Browser",
    manufactureDate: "1995-12-04T12:00:00",
    isAvailable: false,
  },
  {
    id: 5,
    title: "Dell XPS",
    category: "Laptops",
    price: 120000.0,
    description: "An ultra-slim laptop with powerful performance.",
    manufactureDate: "2023-04-25T09:15:00",
    isAvailable: true,
  },
];

let product1 = products.map((value, i) => {
  return value.title;
});
console.log(product1);
//[ 'MacBook Pro', 'Nike', 'Python', 'Javascript', 'Dell XPS' ]

let stat1 = products.map((value, id) => {
  return `${value.title} costs Nrs ${value.price} and its category is ${value.category}`;
});
console.log(stat1);
/*
'MacBook Pro costs Nrs 100000 and its category is Laptops',
  'Nike costs Nrs 5000 and its category is Running Shoes',
  'Python costs Nrs 500 and its category is Books',
  'Javascript costs Nrs 700 and its category is Books',
  'Dell XPS costs Nrs 120000 and its category is Laptops' 
 */

let _arr = products.filter((value, i) => {
  if (value.price > 2000) {
    return true;
  }
});
let stat = _arr.map((value, i) => {
  return `${value.title} costs Nrs ${value.price} and its category is ${value.category}`;
});
console.log(stat);
/* 
  'MacBook Pro costs Nrs 100000 and its category is Laptops',
  'Nike costs Nrs 5000 and its category is Running Shoes',
  'Dell XPS costs Nrs 120000 and its category is Laptops'
   */

let _book = products.filter((value, i) => {
  if (value.category == "Books") {
    return true;
  }
});
let product = _book.map((value, i) => {
  return `${value.title} costs Nrs ${value.price} and its category is ${value.category}`;
});
console.log(product);
/*
  'Python costs Nrs 500 and its category is Books',
  'Javascript costs Nrs 700 and its category is Books'
 */
