import { Review } from "../schema/model.js";

export let createReviewService = async (data) => {
  return await Review.create(data);
};

export let readAllReviewService = async () => {
  return await Review.find({})
    .populate("userId", "name")
    .populate("productId", "name price");
};

export let readSpecificReviewService = async (id) => {
  return await Review.findById(id);
};

export let updateReviewService = async (id, data) => {
  return await Review.findByIdAndUpdate(id, data, {
    new: true,
  });
};

export let deleteReviewService = async (id) => {
  return await Review.findByIdAndDelete(id);
};
