import mongoose from "mongoose"; //Imported from Package.
import { port } from "../constant.js"; //Imported from file.

const connectToMongoDb = () => {
  mongoose.connect(port);
  console.log("Application Is  Connected To Mongodb Successfully.");
};
export default connectToMongoDb;
