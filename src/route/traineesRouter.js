import { Router } from "express";
export let traineesRouter = Router();

traineesRouter
  .route("/") //localhost:8000
  .post((req, res, next) => {
    res.json("i am post method");
  })
  .get((req, res, next) => {
    res.json("i am get method");
  })
  .patch((req, res, next) => {
    res.json("i am patch method");
  })
  .delete((req, res, next) => {
    res.json("i am delete method");
  });
