import { Router } from "express";

export let bikeRouter = Router();
bikeRouter
  .route("/") //localhost:8000/bike

  .post(
    (req, res, next) => {
      console.log("i am normal  middleware 1");
      
      let error = new Error("this is my error")
      next(error) // next with value then it triggers error middleware
    },

    (error, req, res, next) => {
      console.log("i am error middleware first");
      console.log(error.message)// to view the error message.
    },

    (req, res, next) => {
      console.log("i am normal middleware 2");
      next(); // only next then it only triggers normal middleware
    }, 
    
    (error, req, res, next) => {
      console.log("I am error middleware second");
    },
    
    (req, res, next) => {
      console.log("i am normal middleware 3");
      res.json({
        //object
        success: true,
        message: "bike created successfully",
      });
    }
  )

  .get((req, res, next) => {
    console.log(req.body); //to take response from postman
    res.json("bike get"); // to print it on postman itself
  })

  .patch((req, res, next) => {
    console.log(req.body);
    res.json("bike update");
  })

  .delete((req, res, next) => {
    res.json("bike delete");
  });

//For localhost:8000/bike/name   /static

bikeRouter
  .route("/name") //localhost:8000/bike/name

  .get((req, res, next) => {
    res.json("bike name get");
  });

bikeRouter
  .route("/:id") //localhost:8000/bike/:id  :is dynamic

  .get((req, res, next) => {
    console.log(req.params);

    res.json(req.query);
  });
/*
  req.params={
    id:1234556
  } 
   */

bikeRouter.route("/:id1/name/:id2").get((req, res, next) => {
  console.log(req.params);
  res.json(req.params);
});
/* 
 {
    "id1": "123",
    "id2": "abc"
    } 
*/
/*
req.query={
  name="sachee"
  age="22"
}

 */
