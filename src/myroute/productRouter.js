import { Router } from "express";
import {
  createProductController,
  deleteProduct,
  readAllProduct,
  readSpecificProduct,
  updateProduct,
} from "../controller/productController.js";
/*
  make a middleware pass a age
  if age is greater then 18 he can create product
  else throw error.
  
  make a middleware pass age, status
  if age is greater than 18 and status is married, he can create product else throw error
   */
let canCreateProduct = (age, isMarried) => {
  return (req, res, next) => {
    if (age >= 18 && isMarried === true) {
      next();
    } else if (age < 18 && isMarried === false) {
      res.status(400).json({
        success: false,
        message: "you cannot create product.",
      });
    }
  };
};

let productRouter = Router();

// let canAddProduct = (role) => {
//   return (req, res, next) => {
//     if (role === "admin") {
//       next();
//     } else if (role === "customer") {
//       res.status(400).json({
//         success: false,
//         message: "you are not allowed to create product.",
//       });
//     }
//   };
// };let check = (req, res, next) => {};

productRouter
  .route("/")
  // .post(createProduct)
  // .post(canAddProduct("customer"),createProductController)
  // .post(age(10),createProductController)
  .post(canCreateProduct("customer"), createProductController)
  .get(readAllProduct);

productRouter
  .route("/:id")

  .get(readSpecificProduct)

  .patch(updateProduct)

  .delete(deleteProduct);

export default productRouter;
