import { Router } from "express";
import {
  createStudent,
  deleteStudent,
  readAllStudent,
  readSpecificStudent,
  updateStudent,
} from "../controller/studentController.js";

let studentRouter = Router();

studentRouter.route("/")
.post(createStudent)
.get(readAllStudent);

studentRouter
  .route("/:id") 
   //localhost:8000/students/65d32bd76f35cde24585fda5
  // let id = req.params.id;
  
  
  .get(readSpecificStudent)

  .patch(updateStudent)

  .delete(deleteStudent);

export default studentRouter;
