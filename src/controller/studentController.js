import { Student } from "../schema/model.js";
import {
  createStudentService,
  deleteStudentService,
  readAllSpecificStudentService,
  readAllStudentService,
  updateStudentService,
} from "../service/studentService.js";

export let createStudent = async (req, res, next) => {
  try {
    let result = await createStudentService(req.body);
    res.status(201).json({
      success: true,
      message: "student created successfully ",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readAllStudent = async (req, res, next) => {
  try {
    let result = await readAllStudentService();
    res.status(200).json({
      success: true,
      message: "Student read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificStudent = async (req, res, next) => {
  try {
    let result = await readAllSpecificStudentService(req.params.id);

    res.status(200).json({
      success: true,
      message: "student read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateStudent = async (req, res, next) => {
  try {
    let result = await updateStudentService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "student update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteStudent = async (req, res, next) => {
  try {
    let result = await deleteStudentService(req.params.id);
    res.status(200).json({
      success: true,
      message: "student deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
