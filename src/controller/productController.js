import {
  createProductService,
  deleteProductService,
  readAllProductService,
  readSpecificProductService,
  updateProductService,
} from "../service/productServices.js";

export let createProductController = async (req, res, next) => {
  try {
    let result = await createProductService(req.body);
    res.status(201).json({
      success: true,
      message: "product created successfully ",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readAllProduct = async (req, res, next) => {
  try {
    let result = await readAllProductService();
    res.status(200).json({
      success: true,
      message: "Product read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificProduct = async (req, res, next) => {
  try {
    let result = await readSpecificProductService(req.params.id);

    res.status(200).json({
      success: true,
      message: "product read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateProduct = async (req, res, next) => {
  try {
    let result = await updateProductService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "product update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteProduct = async (req, res, next) => {
  try {
    let result = await deleteProductService(req.params.id);
    res.status(200).json({
      success: true,
      message: "product deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
