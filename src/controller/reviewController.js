import {
  createReviewService,
  deleteReviewService,
  readAllReviewService,
  readSpecificReviewService,
  updateReviewService,
} from "../service/reviewServices.js";

export let createReviewController = async (req, res, next) => {
  try {
    let result = await createReviewService(req.body);
    res.status(201).json({
      success: true,
      message: "review created successfully ",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readAllReviewController = async (req, res, next) => {
  try {
    let result = await readAllReviewService();
    res.status(200).json({
      success: true,
      message: "Review read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificReviewController = async (req, res, next) => {
  try {
    let result = await readSpecificReviewService(req.params.id);

    res.status(200).json({
      success: true,
      message: "review read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateReviewController = async (req, res, next) => {
  try {
    let result = await updateReviewService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "review update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteReviewController = async (req, res, next) => {
  try {
    let result = await deleteReviewService(req.params.id);
    res.status(200).json({
      success: true,
      message: "review deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
