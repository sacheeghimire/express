import {
  createUserService,
  deleteUserService,
  readAllUserService,
  readSpecificUserService,
  updateUserService,
} from "../service/userServices.js";

export let createUserController = async (req, res, next) => {
  try {
    let result = await createUserService(req.body);
    res.json({
      success: true,
      message: "user created successfully ",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let readAllUserController = async (req, res, next) => {
  try {
    let result = await readAllUserService();
    res.json({
      success: true,
      message: "User read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificUserController = async (req, res, next) => {
  try {
    let result = await readSpecificUserService(req.params.id);

    res.json({
      success: true,
      message: "user read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateUserController = async (req, res, next) => {
  try {
    let result = await updateUserService(req.params.id, req.body);
    res.json({
      success: true,
      message: "user update successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteUserController = async (req, res, next) => {
  try {
    let result = await deleteUserService(req.params.id);
    res.json({
      success: true,
      message: "user deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
