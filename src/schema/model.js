//define array => modelObject
//name,object
//student=[{name:"Sachee",age:22,isMarried:false}]

import { model } from "mongoose";
import bookSchema from "./bookSchema.js";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import productSchema from "./productSchema.js";
import userSchema from "./userSchema.js";
import reviewSchema from "./reviewSchema.js";

export let Student = model("Student", studentSchema);

export let Teacher = model("Teacher", teacherSchema);

export let Book = model("Book", bookSchema);
export let Product = model("Product", productSchema);
export let User = model("User", userSchema);
export let Review = model("Review", reviewSchema);
