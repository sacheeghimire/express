import { Schema } from "mongoose";

let userSchema = Schema(
  {
    name: {
      required: true,
      type: String,
    },
    address: {
      required: true,
      type: String,
    },
  },
  { timestamps: true }
);
export default userSchema;
