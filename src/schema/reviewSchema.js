import { Schema } from "mongoose";

let reviewSchema = Schema(
  {
    userId: {
      required: true,
      type: Schema.ObjectId,
      ref: "User",
    },
    productId: {
      required: true,
      type: Schema.ObjectId,
      ref: "Product",
    },
    message: {
      required: true,
      type: String,
    },
    rating: {
      required: true,
      type: Number,
    },
  },
  { timestamps: true }
);
export default reviewSchema;
