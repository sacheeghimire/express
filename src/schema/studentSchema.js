import { Schema } from "mongoose";

let studentSchema = Schema(
  {
    name: {
      type: String,
    },
    password: {
      type: String,
    },

    roll: {
      type: Number,
    },
    isMarried: {
      type: Boolean,
    },
    spouseName: {
      type: String,
    },
    email: {
      type: String,
    },
    gender: {
      type: String,
    },
    DOB: {
      type: Date,
    },
    location: {
      country: {
        type: String,
      },
      exactLocation: {
        type: String,
      },
    },
    favTeacher: [{ type: String }],
    favSubject: [
      {
        bookName: {
          type: String,
        },
        bookAuthor: {
          type: String,
        },
      },
    ],
  },
  { timestamps: true }
);
export default studentSchema;
