/*
******  CRUD    *****
(method)
C-Create => post
R-Read   => get
U-Update => patch
D-Delete => delete

CRUD is always related to DB.
 
------------------
Two imp aspects in backend:
*url="localhost:8000"
*method:'___'

*/

/* 
Always place dynamic Route at the end. { :___ }
 */

/*

 localhost:8000/a/b?name=sachee&age=22&address=kathmandu
url = route + query
route =localhost:8000/a/b
route = baseUrl+ route params
baseUrl = localhost:8000
query =name=sachee&age=22&address=kathmandu

 */

/*
We can send data from postman by three way
*body
*params
*query 
 */

//always place dynamic route at the end.

//Whatever we send in URL, everything will come in string.

// One request must have one response.

/*
middleware 
    :middleware are the function which has req, res,next.
    next is used to trigger another middleware.

    We have two forms of middleware:

        error middleware(err,req,res,next)=>{}
            -to trigger error middleware we have to call next(data)
        normal middleware (req,res,next)=>{}
            -to trigger normal middleware we have to call next()
            
    middleware is divided into two parts:
        route middleware
        application middleware
 */
//error let error = new Error("this is my first error")
// throw error;

/*
******model convention (DATABASE)******
  -first letter of the model name must be capital and singular  
  -variable name and model name must be same.
  
  route convention
    at index it is good to use plural routes.
 */

// ** req.body  ** student.create(data)

// Error arises : Promise keyword
// Asynchronous code, to make it run at the line then use await and async in func.


//******************************** */

/*
Product(name,price,quantity)
schema
model
services
controller
route
index 
 */

/*
status code
success (2XX)
c 201
r 200
u 201
d 200

failure(4XX)
 */


/*
{
    "name": "c",
    "password":"Password@123",
    "phoneNumber": 1111111111,
    "roll": 2,
    "isMarried": false,
    "spouseName": "lkjl",
    "email": "abc@gmai",
    "gender": "male",
    "dob": "2019-2-2",
    "location": {
        "country": "nepal",
        "exactLocation": "gagalphedi"
    },
    "favTeacher": [
        "a",
        "b",
        "c",
        "nitan"
    ],



    "favSubject": [
        {
            "bookName": "javascript",
            "bookAuthor": "nitan"
        },
        {
            "bookName": "b",
            "bookAuthor": "b"
        }
    ],



} 
 */